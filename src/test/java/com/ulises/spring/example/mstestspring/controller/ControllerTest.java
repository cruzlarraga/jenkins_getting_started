package com.ulises.spring.example.mstestspring.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
//import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(TestController.class)
public class ControllerTest {

	@Autowired
    private MockMvc mvc;
	
	@Test
	public void testController() throws Exception {
		mvc.perform( get("/api/v1/test")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk());
	}
}
