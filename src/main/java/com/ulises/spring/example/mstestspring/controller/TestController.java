package com.ulises.spring.example.mstestspring.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1")
public class TestController {

    @RequestMapping(value = "/test")
    public String testController(){

        return "Hello world Spring Boot";

    }

}