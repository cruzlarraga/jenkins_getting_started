package com.ulises.spring.example.mstestspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsTestSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsTestSpringApplication.class, args);
	}

}