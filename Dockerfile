FROM java:8-jre
MAINTAINER Ulises Lárraga <uliseslarraga@gmail.com>

ADD ./target/ms-test-spring-0.0.1.jar /app/
CMD ["java", "-Xmx200m", "-jar", "/app/ms-test-spring-0.0.1.jar"]
